<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('part')->nullable();
            $table->integer('test')->nullable();
            $table->char('key',1)->nullable();
            $table->integer('book_id')->nullable();
            $table->string('question_img')->index()->nullable();
            $table->string('answer_img')->index()->nullable();
            $table->string('slug')->index()->nullable();
            $table->string('description')->index()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
