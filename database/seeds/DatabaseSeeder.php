<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        for($j=1;$j<=10;$j++)
        {
            for($i = 101;$i<=130;$i++){
                factory(App\Models\Questions::class, 1)->create([
                    'part' => '5',
                    'test' => $j,
                    'book_id' => '1',
                    'question_img' => 'images/part5/'.'b1p5t'.$j.'q'. $i.'.PNG',
                    'answer_img' => 'images/part5/'.'b1p5t'.$j.'a'. $i.'.PNG',
                ]);
            }

            for($i = 1;$i<=4;$i++){
                factory(App\Models\Questions::class, 1)->create([
                    'part' => '6',
                    'test' => $j,
                    'book_id' => '1',
                    'question_img' => 'images/part6/'.'b1p6t'.$j.'q'. $i.'.PNG',
                    'answer_img' => 'images/part6/'.'b1p6t'.$j.'a'. $i.'.PNG',
                ]);
            }
        }

        
    }
}
