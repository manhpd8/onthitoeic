<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.home');
});
Route::get('/part5/{mix}/{num_of_page}','QuestionsController@part5');
Route::get('/part6/{mix}/{num_of_page}','QuestionsController@part6');
