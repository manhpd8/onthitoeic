<section class="section-content padding-y-sm bg">
<div class="container">

<header class="section-heading heading-line">
	<h4 class="title-section bg"></h4>
</header>

<div class="card">
<div class="row no-gutters">
	<div class="col-md-3">
	
<article href="#" class="card-banner h-100 bg2">
	<div class="card-body zoom-wrap">
		<h5 class="title">{{$cat1->name}}</h5>
		<p>{{$cat1->description}}</p>
		<a href="#" class="btn btn-warning">Xem Thêm</a>
		<img src="front/images/items/item-sm.png" height="200" class="img-bg zoom-in">
	</div>
</article>

	</div> <!-- col.// -->
	<div class="col-md-9">
		<ul class="row no-gutters border-cols">
			@foreach($pros_cat1 as $pro_cat1)
			<li class="col-6 col-md-3">
				<a href="#" class="itembox"> 
					<div class="card-body">
						<p class="word-limit">{{$pro_cat1->name}}</p>
						<img class="img-sm" src="{{asset($pro_cat1->icon)}}">
					</div>
				</a>
			</li>
			@endforeach
		</ul>
	</div> <!-- col.// -->
</div> <!-- row.// -->
	
</div> <!-- card.// -->

</div> <!-- container .//  -->
</section>