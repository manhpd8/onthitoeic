@extends('layouts.app')
@section('title', "title here")
@section('styles')

@endsection


@section('content')
<!-- ========================= SECTION CONTENT ========================= -->
<section class="section-content bg padding-y">
<div class="container">

<div class="row">
	<main class="col-sm-12" style="align-content: center;">
		@foreach($questions as $question)
		<article class="card card-product">
			<div class="card-body">
			<div class="row">
				<img src="{{secure_asset($question->question_img)}}" class="img-fluid">
			</div> <!-- row.// -->
			</div> <!-- card-body .// -->
		</article> <!-- card product .// -->
		<div class="row">
			<dl class="dlist-inline">
			  <dt>Question 1: </dt>
			  <dd> 
			  	<select class="form-control form-control-sm" style="width:70px;">
			  		<option> A </option>
			  		<option> B </option>
			  		<option> C </option>
			  		<option> D </option>
			  	</select>
			  </dd>
			</dl>  <!-- item-property .// -->
			<dl class="dlist-inline">
			  <dt>Question 2: </dt>
			  <dd> 
			  	<select class="form-control form-control-sm" style="width:70px;">
			  		<option> A </option>
			  		<option> B </option>
			  		<option> C </option>
			  		<option> D </option>
			  	</select>
			  </dd>
			</dl>  <!-- item-property .// -->
			<dl class="dlist-inline">
			  <dt>Question 3: </dt>
			  <dd> 
			  	<select class="form-control form-control-sm" style="width:70px;">
			  		<option> A </option>
			  		<option> B </option>
			  		<option> C </option>
			  		<option> D </option>
			  	</select>
			  </dd>
			</dl>  <!-- item-property .// -->
			<dl class="dlist-inline">
			  <dt>Question 4: </dt>
			  <dd> 
			  	<select class="form-control form-control-sm" style="width:70px;">
			  		<option> A </option>
			  		<option> B </option>
			  		<option> C </option>
			  		<option> D </option>
			  	</select>
			  </dd>
			</dl>  <!-- item-property .// -->
		</div> <!-- col.// -->
		<button class="btn  btn-danger" onclick="funViewAnswer()">View Answer</button>
		<article class="card card-product" style="display: none" id="viewAnswer">
			<div class="card-body">
			<div class="row">
				<img src="{{secure_asset($question->answer_img)}}" class="img-fluid">
			</div> <!-- row.// -->
			</div> <!-- card-body .// -->
		</article> <!-- card product .// -->
		@endforeach
		{{$questions->links()}}
	</main> <!-- col.// -->
</div>

</div> <!-- container .//  -->
</section>
<!-- ========================= SECTION CONTENT END// ========================= -->

@endsection


@section('scripts')
<script type="text/javascript">
	function funViewAnswer(){
		var answer = document.getElementById("viewAnswer");
		if(answer.style.display == "none"){
			answer.style.display = "block";
		} else{
			answer.style.display = "none";
		}
		
		var yourAnswer = document.getElementById("viewAnswer");
	}
</script>
@endsection