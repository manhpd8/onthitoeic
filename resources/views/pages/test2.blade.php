@extends('layouts.app')
@section('title', "title here")
@section('content')
<div class="owl-carousel owl-init slide-items owl-loaded owl-drag" id="slide_custom_nav" data-custom-nav="custom-nav-first" data-items="5" data-margin="20" data-dots="true" data-nav="false">
	
	
	
	
	
	
<div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-1170px, 0px, 0px); transition: all 0s ease 0s; width: 4680px;"><div class="owl-item cloned" style="width: 370px; margin-right: 20px;"><div class="item-slide">
<figure class="card card-product">
	<div class="img-wrap"> <img src="images/items/4.jpg"> </div>
	<figcaption class="info-wrap text-center">
		<h6 class="title text-truncate"><a href="#">Good item name</a></h6>
	</figcaption>
</figure> <!-- card // -->
	</div></div><div class="owl-item cloned" style="width: 370px; margin-right: 20px;"><div class="item-slide">
<figure class="card card-product">
	<div class="img-wrap"> <img src="images/items/5.jpg"> </div>
	<figcaption class="info-wrap text-center">
		<h6 class="title text-truncate"><a href="#">Another item name</a></h6>
	</figcaption>
</figure> <!-- card // -->
	</div></div><div class="owl-item cloned" style="width: 370px; margin-right: 20px;"><div class="item-slide">
<figure class="card card-product">
	<div class="img-wrap"> <img src="images/items/3.jpg"> </div>
	<figcaption class="info-wrap text-center">
		<h6 class="title text-truncate"><a href="#">Third item name</a></h6>
	</figcaption>
</figure> <!-- card // -->
	</div></div><div class="owl-item active" style="width: 370px; margin-right: 20px;"><div class="item-slide">
<figure class="card card-product">
	<span class="badge-new"> NEW </span>
	<div class="img-wrap"> <img src="images/items/1.jpg"> </div>
	<figcaption class="info-wrap text-center">
		<h6 class="title text-truncate"><a href="#">First item name</a></h6>
	</figcaption>
</figure> <!-- card // -->
	</div></div><div class="owl-item" style="width: 370px; margin-right: 20px;"><div class="item-slide">
<figure class="card card-product">
	<div class="img-wrap"> <img src="images/items/2.jpg"> </div>
	<figcaption class="info-wrap text-center">
		<h6 class="title text-truncate"><a href="#">Second item name</a></h6>
	</figcaption>
</figure> <!-- card // -->
	</div></div><div class="owl-item" style="width: 370px; margin-right: 20px;"><div class="item-slide">
<figure class="card card-product">
	<div class="img-wrap"> <img src="images/items/3.jpg"> </div>
	<figcaption class="info-wrap text-center">
		<h6 class="title text-truncate"><a href="#">Third item name</a></h6>
	</figcaption>
</figure> <!-- card // -->
	</div></div><div class="owl-item" style="width: 370px; margin-right: 20px;"><div class="item-slide">
<figure class="card card-product">
	<div class="img-wrap"> <img src="images/items/4.jpg"> </div>
	<figcaption class="info-wrap text-center">
		<h6 class="title text-truncate"><a href="#">Good item name</a></h6>
	</figcaption>
</figure> <!-- card // -->
	</div></div><div class="owl-item" style="width: 370px; margin-right: 20px;"><div class="item-slide">
<figure class="card card-product">
	<div class="img-wrap"> <img src="images/items/5.jpg"> </div>
	<figcaption class="info-wrap text-center">
		<h6 class="title text-truncate"><a href="#">Another item name</a></h6>
	</figcaption>
</figure> <!-- card // -->
	</div></div><div class="owl-item" style="width: 370px; margin-right: 20px;"><div class="item-slide">
<figure class="card card-product">
	<div class="img-wrap"> <img src="images/items/3.jpg"> </div>
	<figcaption class="info-wrap text-center">
		<h6 class="title text-truncate"><a href="#">Third item name</a></h6>
	</figcaption>
</figure> <!-- card // -->
	</div></div><div class="owl-item cloned" style="width: 370px; margin-right: 20px;"><div class="item-slide">
<figure class="card card-product">
	<span class="badge-new"> NEW </span>
	<div class="img-wrap"> <img src="images/items/1.jpg"> </div>
	<figcaption class="info-wrap text-center">
		<h6 class="title text-truncate"><a href="#">First item name</a></h6>
	</figcaption>
</figure> <!-- card // -->
	</div></div><div class="owl-item cloned" style="width: 370px; margin-right: 20px;"><div class="item-slide">
<figure class="card card-product">
	<div class="img-wrap"> <img src="images/items/2.jpg"> </div>
	<figcaption class="info-wrap text-center">
		<h6 class="title text-truncate"><a href="#">Second item name</a></h6>
	</figcaption>
</figure> <!-- card // -->
	</div></div><div class="owl-item cloned" style="width: 370px; margin-right: 20px;"><div class="item-slide">
<figure class="card card-product">
	<div class="img-wrap"> <img src="images/items/3.jpg"> </div>
	<figcaption class="info-wrap text-center">
		<h6 class="title text-truncate"><a href="#">Third item name</a></h6>
	</figcaption>
</figure> <!-- card // -->
	</div></div></div></div><div class="owl-nav disabled"><button type="button" role="presentation" class="owl-prev"><i class="fa fa-chevron-left"></i></button><button type="button" role="presentation" class="owl-next"><i class="fa fa-chevron-right"></i></button></div><div class="owl-dots"><button role="button" class="owl-dot active"><span></span></button><button role="button" class="owl-dot"><span></span></button><button role="button" class="owl-dot"><span></span></button><button role="button" class="owl-dot"><span></span></button><button role="button" class="owl-dot"><span></span></button><button role="button" class="owl-dot"><span></span></button></div></div>
@endsection