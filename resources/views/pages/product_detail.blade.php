@extends('layouts.app')
@section('title', "title here")
@section('styles')

@endsection


@section('content')

<!-- ========================= SECTION CONTENT ========================= -->
<section class="section-content bg padding-y-sm">
<div class="container">
<nav class="mb-3">
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="front/#">Trang chủ</a></li>
    <li class="breadcrumb-item"><a href="front/#">Sản phẩm</a></li>
    <li class="breadcrumb-item active" aria-current="page" >{{$product->name}}</li>
</ol> 
</nav>

<div class="row">
<div class="col-xl-10 col-md-9 col-sm-12">


<main class="card">
	<div class="row no-gutters">
		<aside class="col-sm-6 border-right">
<article class="gallery-wrap"> 
<div class="img-big-wrap">
  <div> <a href="{{asset($product->icon)}}" data-fancybox=""><img src="{{asset($product->icon)}}"></a></div>
</div> <!-- slider-product.// -->
<div class="img-small-wrap">
  <div class="item-gallery"> <img src="{{asset($product->icon)}}"></div>
  <div class="item-gallery"> <img src="{{asset($product->icon)}}"></div>
  <div class="item-gallery"> <img src="{{asset($product->icon)}}"></div>
  <div class="item-gallery"> <img src="{{asset($product->icon)}}"></div>
</div> <!-- slider-nav.// -->
</article> <!-- gallery-wrap .end// -->
		</aside>
		<aside class="col-sm-6">
<article class="card-body">
<!-- short-info-wrap -->
	<h3 class="title mb-3" style="color: #155415; font-weight: 700;">{{$product->name}}</h3>

<div class="mb-3"> 
	<var class="price h3 text-warning"> 
		<span class="num">{{number_format($product->price)}}</span>
		<span class="currency">Vnd</span>
		<span class="num" style="text-decoration-line: line-through; font-size: 17px">{{number_format($product->price_old)}} vnd</span>
	</var> 
	<span>/kg</span> 
</div> <!-- price-detail-wrap .// -->
<dl>
  <dt>Mô tả sản phẩm</dt>
  <dd><p>{{$product->description}}</p></dd>
</dl>
<dl class="row">
  <dt class="col-sm-3">Loại:</dt>
  <dd class="col-sm-9">{{$product->category->name}}</dd>

  <dt class="col-sm-3">Xuất xứ:</dt>
  <dd class="col-sm-9">Việt Nam </dd>

</dl>
<div class="rating-wrap">

	<ul class="rating-stars">
		<li style="width:80%" class="stars-active"> 
			<i class="fa fa-star"></i> <i class="fa fa-star"></i> 
			<i class="fa fa-star"></i> <i class="fa fa-star"></i> 
			<i class="fa fa-star"></i> 
		</li>
		<li>
			<i class="fa fa-star"></i> <i class="fa fa-star"></i> 
			<i class="fa fa-star"></i> <i class="fa fa-star"></i> 
			<i class="fa fa-star"></i> 
		</li>
	</ul>
	<div class="label-rating">132 reviews</div>
	<div class="label-rating">154 orders </div>
</div> <!-- rating-wrap.// -->
<hr>
	<div class="row">
		<div class="col-sm-5">
			<dl class="dlist-inline">
			  <dt>Số lượng: </dt>
			  <dd> 
			  	<select class="form-control form-control-sm" style="width:70px;">
			  		<option> 1 </option>
			  		<option> 2 </option>
			  		<option> 3 </option>
			  	</select>
			  </dd>
			</dl>  <!-- item-property .// -->
		</div> <!-- col.// -->
		<div class="col-sm-7">
			<dl class="dlist-inline">
				  <dt>Kích thước: </dt>
				  <dd>
				  	<label class="form-check form-check-inline">
					  <input class="form-check-input" name="inlineRadioOptions" id="inlineRadio2" value="option2" type="radio">
					  <span class="form-check-label">to</span>
					</label>
				  </dd>
				  <dd>
				  	<label class="form-check form-check-inline">
					  <input class="form-check-input" name="inlineRadioOptions" id="inlineRadio2" value="option2" type="radio">
					  <span class="form-check-label">vừa</span>
					</label>
				  </dd>
			</dl>  <!-- item-property .// -->
		</div> <!-- col.// -->
	</div> <!-- row.// -->
	<hr>
	<a href="front/#" class="btn  btn-warning"> <i class="fa fa-envelope"></i> Liên hệ ngay </a>
	<a href="front/#" class="btn  btn-outline-warning"> Thêm giỏ hàng </a>
<!-- short-info-wrap .// -->
</article> <!-- card-body.// -->
		</aside> <!-- col.// -->
	</div> <!-- row.// -->
</main> <!-- card.// -->

<!-- PRODUCT DETAIL -->
<article class="card mt-3">
	<div class="card-body">
		<h4>Thông tin chi tiết sản phẩm</h4>
	<p>{{$product->description}}</p>
	</div> <!-- card-body.// -->
</article> <!-- card.// -->

<!-- PRODUCT DETAIL .// -->

</div> <!-- col // -->
<aside class="col-xl-2 col-md-3 col-sm-12">
<div class="card">
	<div class="btn btn-success btn-block" >
		  	<span class="form-check-label">Sản phẩm tương tự</span>
	</div>
	<div class="card-body row">
		@foreach($likeProduct as $likeprod)
			<div class="col-md-12 col-sm-3">
				<figure class="item border-bottom mb-3">
						<a href="front/#" class="img-wrap"> <img src="{{asset($likeprod->icon)}}" class="img-md"></a>
						<figcaption class="info-wrap">
							<a href="front/#" class="title">{{$likeprod->name}}</a>
							<div class="price-wrap mb-3">
								<span class="price-new">${{$likeprod->price}}</span> <del class="price-old">${{$likeprod->price_old}}</del>
							</div> <!-- price-wrap.// -->
						</figcaption>
				</figure> <!-- card-product // -->
			</div> <!-- col.// -->
		@endforeach
	</div> <!-- card-body.// -->
</div> <!-- card.// -->

</aside> <!-- col // -->
</div> <!-- row.// -->



</div><!-- container // -->
</section>
<!-- ========================= SECTION CONTENT .END// ========================= -->
@endsection


@section('scripts')

@endsection