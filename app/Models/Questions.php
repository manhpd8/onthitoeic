<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{
    public function category()
    {
    	 $table = 'questions';
    	return $this->belongsTo('App\Models\Books','book_id','id');
    }
}
