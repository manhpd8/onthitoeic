<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Questions;
use DB;
class QuestionsController extends Controller
{
    public function part5(Request $request,$mix ='asc',$num_of_page = 1)
    {

        $part = $request->part;
        $mix = $request->mix;
        $page = $request->num_of_page;
        $questions = DB::table('questions')->where('part','5');
        if($mix =='asc') {
        	$questions = $questions->orderBy('question_img', 'asc');
        }
        if($mix =='desc') {
            $questions = $questions->orderBy('question_img', 'desc');
        }
        if($mix =='random') {
            $questions = $questions->inRandomOrder();
        }
        if($page == null || $page <=0 ){
        	$page =1;
        }
        $questions = $questions->paginate($page);
        $data['questions'] = $questions;

        return view('pages.part5', $data);
    }

    public function part6(Request $request,$mix ='asc',$num_of_page = 1)
    {

        $part = $request->part;
        $mix = $request->mix;
        $page = $request->num_of_page;
        $questions = DB::table('questions')->where('part','6');
        if($mix =='asc') {
            $questions = $questions->orderBy('question_img', 'asc');
        }
        if($mix =='desc') {
            $questions = $questions->orderBy('question_img', 'desc');
        }
        if($mix =='random') {
            $questions = $questions->inRandomOrder();
        }
        if($page == null || $page <=0 ){
            $page =1;
        }
        $questions = $questions->paginate($page);
        $data['questions'] = $questions;

        return view('pages.part6', $data);
    }
}
